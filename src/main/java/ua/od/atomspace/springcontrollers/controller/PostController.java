package ua.od.atomspace.springcontrollers.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.od.atomspace.springcontrollers.dao.model.Post;
import ua.od.atomspace.springcontrollers.dao.repository.PostRepository;
import ua.od.atomspace.springcontrollers.exceptions.ResourceNotFoundException;

@RequestMapping("api/posts")
@RestController
@Slf4j
public class PostController {
    private final PostRepository postRepository;

    @Autowired
    public PostController(PostRepository postRepository){this.postRepository = postRepository;}

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<Post> list() {
        Iterable<Post> response = postRepository.findAll();
        log.info("get all posts {}", response);
        return response;
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post get(@PathVariable Long id) throws ResourceNotFoundException {
        Post response =  postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        log.info("get post {}", response);
        return response;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public Post post(@RequestBody Post request) {
        Post response = postRepository.save(request);
        log.info("created post {}", response);
        return response;
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post update(@RequestBody Post request, @PathVariable(name = "id") Long id) throws ResourceNotFoundException{
        Post post = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        post.setText(request.getText());
        post.setTitle(request.getTitle());

        Post response = postRepository.save(post);
        log.info("update post {}", response);
        return  response;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Post delete(@PathVariable Long id) throws ResourceNotFoundException {
        Post response = postRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        postRepository.delete(response);
        log.info("delete post {}", response);
        return response;
    }
}
