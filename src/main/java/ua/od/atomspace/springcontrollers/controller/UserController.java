package ua.od.atomspace.springcontrollers.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.od.atomspace.springcontrollers.dao.model.User;
import ua.od.atomspace.springcontrollers.dao.repository.UserRepository;
import ua.od.atomspace.springcontrollers.exceptions.ResourceNotFoundException;

@RequestMapping("api/users")
@RestController
@Slf4j
public class UserController {
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<User> list() {
        Iterable<User> response = userRepository.findAll();
        log.info("get all users {}", response);
        return response;
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User get(@PathVariable Long id) throws ResourceNotFoundException {
        User response =  userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        log.info("get user {}", response);
        return response;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public User post(@RequestBody User request) {
        User response = userRepository.save(request);
        log.info("create user {}", response);
        return response;
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User update(@RequestBody User request, @PathVariable(name = "id") Long id) throws ResourceNotFoundException{
        User user = userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        user.setAge(request.getAge());
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setPassword(request.getPassword());
        user.setLastName(request.getLastName());
        user.setUsername(request.getUsername());

        User response = userRepository.save(user);
        log.info("update user {}", response);
        return response;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public User delete(@PathVariable Long id) throws ResourceNotFoundException {
        User response = userRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        log.info("delete user {}", response);
        userRepository.delete(response);
        return response;
    }

    @GetMapping(params = {"ageGreaterThan"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<User>  listByAgeGreaterThan(@RequestParam Integer ageGreaterThan) {
        Iterable<User> response = userRepository.findAllByAgeGreaterThan(ageGreaterThan);
        log.info("get all by age greater than {}", ageGreaterThan);
        return response;
    }

}
