package ua.od.atomspace.springcontrollers.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.springcontrollers.dao.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAllByAgeGreaterThan(Integer age);
}
