package ua.od.atomspace.springcontrollers.dao.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    @Setter
    private long id;

    @Column(name = "title", length = 50)
    @Getter
    @Setter
    private String title;

    @Column(name = "text", length = 200)
    @Getter
    @Setter
    private String text;

    @Column(name = "created_at")
    @Getter
    @Setter
    private Date createdAt;
}
