package ua.od.atomspace.springcontrollers.dao.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    @Getter
    @Setter
    private long id;

    @Column(name = "first_name", length = 30)
    @Getter
    @Setter
    private String firstName;

    @Column(name = "last_name", length = 30)
    @Getter
    @Setter
    private String lastName;

    @Column(name = "age")
    @Getter
    @Setter
    private Integer age;

    @Column(name = "email", length = 50)
    @Getter
    @Setter
    private String email;

    @Column(name = "username", length = 50)
    @Getter
    @Setter
    private String username;

    @Column(name = "password", length = 50)
    @Getter
    @Setter
    private String password;

    @Column(name = "created_at")
    @Getter
    @Setter
    private Date createdAt;
}
