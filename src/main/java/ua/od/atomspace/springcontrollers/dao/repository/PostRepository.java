package ua.od.atomspace.springcontrollers.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.od.atomspace.springcontrollers.dao.model.Post;

public interface PostRepository extends CrudRepository<Post, Long> {
}
